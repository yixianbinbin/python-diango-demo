from django.http import HttpResponse
import json

class RequestUtil:
    def getJsonRequest(self,code,msg,parameter,data):
        if not parameter:
            parameter = {}
        if not data:
            data = []
        result = {}
        result['code'] = code
        result['msg'] = msg
        result['parameter'] = parameter
        result['data'] = data
        return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json,charset=utf-8")
