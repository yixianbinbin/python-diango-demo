from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
import json, requests
from django1.myutil.RequestUtil import *


def catch_exception(origin_func):
    def wrapper(request,*args, **kwargs):
        try:
            print(origin_func.__name__)
            token = request.GET.get('token')
            if not token:
                token = request.POST.get('token')
            print(token)
            print(request.META.get('wsgi.url_scheme'))
            u = origin_func(request,*args, **kwargs)
            return u
        except Exception:
            return 'an Exception raised.'
    return wrapper


def http_test(request):
    r = requests.get('https://www.baidu.com')
    r.encoding = "utf-8"
    return HttpResponse(r.text)

@catch_exception
def ret_json(request):
    result = [{"name": "yiige"}]
    return RequestUtil.getJsonRequest(None, 0, "successful", None, result)


def homePage(request):
    user_input = []
    user = request.GET.get('user', None)
    email = request.GET.get('email', None)
    temp = {'user': user, 'email': email}
    user_input.append(temp)
    return render(request, 'view/login.html', {'data': user_input})


def helloWorld(request):
    test = request.GET.get('test', 'test')
    return HttpResponse("Hello world! " + test)
